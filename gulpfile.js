/*
* Dependencias
*/
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify');
  var webserver = require('gulp-webserver');
  var imagemin = require('gulp-imagemin');
	clean = require('gulp-rimraf');
	sass        = require('gulp-sass'),
    sourcemaps  = require('gulp-sourcemaps'),



// Copia de los cambios en los ficheros html en el directorio dist.
gulp.task('html', function() {
    return gulp.src(['src/*.html'])
        .pipe(gulp.dest('dist/html/'))
});

// Copia de los cambios en los ficheros html, scss y js.
gulp.task('copy', ['html', 'css', 'js']);

/* 
* Procesamiento de imágenes para comprimir / optimizar las mismas.
*/ 

gulp.task('imagenes', function () {
    return gulp.src(['src/img/*.*'])
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images/'));
});

// Limpieza del directorio dist
gulp.task('limpiar', function() {
    return gulp.src(['../dist/js/*.js'], { read: false 
})
           .pipe(clean({ force: true }));
});

gulp.task('lint', function() {
  return gulp.src(['src/js/*.js'])
    .pipe(jshint())
    .pipe(gulp.dest('dist/js/'));
});

/* 
* Procesamiento de ficheros SCSS 
*/ 
gulp.task('css', function() {
    return gulp.src(['src/scss/.base.scss'])
        .pipe(gulp.dest('dist/scss/'))
});

/* 
* Procesamiento de ficheros JS 
*/ 
gulp.task('js', function() {
    return gulp.src(['src/js/main.js', 'src/js/extra.js']) 
        .pipe(gulp.dest('dist/scripts/'))
        
});

/*
* Tarea para lanzar el proceso de servidor 
*/
gulp.task('serve',['limpiar','html', 'css', 'js', 'copy', 'imagenes'], function () {
  return gulp.src(['src/'])
    .pipe(webserver({
       logLevel: "info",
        browser: ["google chrome", "Firefox"],
        proxy: "localhost:8080",
        startPath: "/gulp-basics/dist/"
    }));
	
	 gulp.watch('src/*.html', ['html']);
    gulp.watch('src/img/*', ['imagemin']);   
    gulp.watch('src/scss/*.scss', ['css']);
    gulp.watch('src/js/*.js', ['js']);
});


/* 
* Definción de la tarea por defecto que en este caso limpia el directorio destino
* y lanza la tarea de servidor.
*/
gulp.task('default', ['limpiar', 'serve'], function() {});


